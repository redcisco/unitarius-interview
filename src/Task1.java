import exception.EmptyArrayException;
import exception.ShiftPositionException;

import java.util.Scanner;

public class Task1 {
    private static final Scanner scanner = new Scanner(System.in);

    public static int[] arrayShift(int[] sourceArray, int shiftPosition){
        int buffer, currIndex, shiftIndex;
        for(int i = 0 ; i < gcd(shiftPosition, sourceArray.length); i++){
            buffer = sourceArray[i];
            currIndex = i;

            while (true){
                shiftIndex = currIndex + shiftPosition;
                if(shiftIndex >= sourceArray.length)
                    shiftIndex = shiftIndex - sourceArray.length;
                if(shiftIndex == i)
                    break;
                sourceArray[currIndex] = sourceArray[shiftIndex];
                currIndex = shiftIndex;
            }
            sourceArray[currIndex] = buffer;
        }
        return sourceArray;
    }


    public static int gcd(int a, int b) {
        return (b == 0) ? a : gcd(b, a % b);
    }


    public static void printArray(int[] array){
        for(int item: array){
            System.out.print(item + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int arraySize;
        int shiftPosition;
        int[] array;
        int[] shiftedArray;

        try{
            System.out.println("Enter array size:");
            arraySize = scanner.nextInt();
            if(arraySize == 0)
                throw new EmptyArrayException();
            array = new int[arraySize];

            System.out.println("Enter shift position:");
            shiftPosition = scanner.nextInt();
            if(shiftPosition > arraySize)
                throw new ShiftPositionException();

            System.out.println("Enter array line:");
            for(int i = 0; i < arraySize; i++){
                array[i] = scanner.nextInt();
            }

            System.out.println("Source array:");
            printArray(array);

            System.out.println("Shifted array:");
            shiftedArray = arrayShift(array, shiftPosition);

            printArray(shiftedArray);
        }catch (ShiftPositionException | EmptyArrayException e){
            System.out.println(e.getMessage());
        }

    }
}
