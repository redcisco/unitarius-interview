package exception;

public class EmptyArrayException extends RuntimeException{
    private static final String MESSAGE = "Entered empty array";
    public EmptyArrayException() {
        super(MESSAGE);
    }
}
