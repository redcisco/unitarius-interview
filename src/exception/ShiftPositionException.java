package exception;

public class ShiftPositionException extends RuntimeException{
    private static final String MESSAGE = "Position greater than the length of the array";
    public ShiftPositionException() {
        super(MESSAGE);
    }
}
